﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
using namespace std::chrono_literals;

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 800), "obama shapes");
    sf::CircleShape circle(100.f);
    circle.setOrigin(100, 100);
    int circle_x = 900;
    int circle_y = 100;
    circle.setPosition(circle_x, circle_y);
    sf::CircleShape triangle(80, 3);
    triangle.setOrigin(40, 40);
    int triangle_x = 777;
    int triangle_y = 300;
    triangle.setPosition(triangle_x, triangle_y);
    sf::CircleShape hexagon(200, 6);
    hexagon.setOrigin(100, 100);
    int hexagon_x = 669;
    int hexagon_y = 505;
    hexagon.setPosition(hexagon_x, hexagon_y);
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        sf::Texture circle_texture;
        circle_texture.loadFromFile("img\\obamacircle.jpg");
        circle.setTexture(&circle_texture);
        circle_x-=3;
        if (circle_x < 100)
            circle_x = 100;
        circle.setPosition(circle_x, circle_y);
        sf::Texture triangle_texture;
        triangle_texture.loadFromFile("img\\obamatriangle.jpg");
        triangle.setTexture(&triangle_texture);
        triangle_x -= 2.1;
        if (triangle_x < 30)
            triangle_x = 30;
        triangle.setPosition(triangle_x, triangle_y);
        sf::Texture hexagon_texture;
        hexagon_texture.loadFromFile("img\\obama6.jpg");
        hexagon.setTexture(&hexagon_texture);
        hexagon_x -= 1.2;
        if (hexagon_x < 80)
            hexagon_x = 80;
        hexagon.setPosition(hexagon_x, hexagon_y);
        
        std::this_thread::sleep_for(1ms);
        window.clear();
        window.draw(circle);
        window.draw(triangle);
        window.draw(hexagon);
        window.display();
    }
    

    return 0;
}